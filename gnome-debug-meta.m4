AC_DEFUN([GNOME_INIT_GNOME_DEBUG_META],[
	meta_srcdir=$ac_cv_gnome_debug_meta_srcdir
	meta_builddir=$ac_cv_gnome_debug_meta_builddir
	. $ac_cv_gnome_debug_meta_srcdir/gnome-debug/GNOME-DEBUG-VERSION
	AC_SUBST(meta_srcdir)
	AC_SUBST(meta_builddir)

	GNOME_DEBUG_LIBDIR='$(libdir)'
	GNOME_DEBUG_INCLUDEDIR='$(includedir)'
	GNOME_DEBUG_LIBS='$(meta_builddir)/gnome-debug/lib/libgnome-debug.la'
	GNOME_DEBUG_INCS='-I$(meta_srcdir)/includes -I$(meta_builddir)/includes'
	AC_SUBST(GNOME_DEBUG_LIBDIR)
	AC_SUBST(GNOME_DEBUG_INCLUDEDIR)
	AC_SUBST(GNOME_DEBUG_EXTRA_LIBS)
	AC_SUBST(GNOME_DEBUG_LIBS)
	AC_SUBST(GNOME_DEBUG_INCS)
	AC_SUBST(GNOME_DEBUG_MAJOR_VERSION)
	AC_SUBST(GNOME_DEBUG_MINOR_VERSION)
	AC_SUBST(GNOME_DEBUG_MICRO_VERSION)
	AC_SUBST(GNOME_DEBUG_VERSION)
	AC_SUBST(GNOME_DEBUG_VERSION_CODE)
	AC_DEFINE(HAVE_GNOME_DEBUG)
	AC_DEFINE_UNQUOTED(GNOME_DEBUG_VERSION, "$GNOME_DEBUG_VERSION")
	AC_DEFINE_UNQUOTED(GNOME_DEBUG_VERSION_CODE, $GNOME_DEBUG_VERSION_CODE)
	AC_DEFINE_UNQUOTED(GNOME_DEBUG_MAJOR_VERSION, $GNOME_DEBUG_MAJOR_VERSION)
	AC_DEFINE_UNQUOTED(GNOME_DEBUG_MINOR_VERSION, $GNOME_DEBUG_MINOR_VERSION)
	AC_DEFINE_UNQUOTED(GNOME_DEBUG_MICRO_VERSION, $GNOME_DEBUG_MICRO_VERSION)
	AM_CONDITIONAL(HAVE_GNOME_DEBUG, true)
])
