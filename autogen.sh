#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="GNOME Debugging Framework"

(test -f $srcdir/configure.in \
  && test -d $srcdir/gnome-debug/doc \
  && test -f $srcdir/gnome-debug/doc/arch.txt) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level $PKG_NAME directory"
    exit 1
}

ACLOCAL_FLAGS="-I `(cd $srcdir && pwd)` $ACLOCAL_FLAGS" . $srcdir/macros/autogen.sh
